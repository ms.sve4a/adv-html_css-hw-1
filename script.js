;$(function() {

    /* START nav */
    let navOpened = false;

    const $nav = $('.nav');
    const $btnMenu = $nav.find('.nav__button');
    const $lines = $btnMenu.find('.nav__lines');
    const $menu = $nav.find('.nav__menu');


    $(document).on('click', (event) => {
        if(
            $(event.target).is(':not(.nav__menu,.nav__button)') &&
            $(event.target).parents().is(':not(.nav__menu,.nav__button)') &&
            navOpened
        ) {
            $btnMenu.click();
        }
    });

    $btnMenu.on('click', function() {
        $lines
            .toggleClass('nav__lines--hamburger')
            .toggleClass('nav__lines--cross');

        if($lines.hasClass('nav__lines--hamburger')) {
            $menu.css({ display: 'none' });
            navOpened = false;
        } else {
            $menu.css({ display: 'flex' });
            navOpened = true;
        }
    });
    /* END nav */


});